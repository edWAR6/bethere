(function() {
// document.addEventListener("deviceready", function(){

	var mouseEventTypes = {
	    touchstart : "mousedown",
	    touchmove : "mousemove",
	    touchend : "mouseup"
	};

	for (originalType in mouseEventTypes) {
	    document.addEventListener(originalType, function(originalEvent) {
	        event = document.createEvent("MouseEvents");
	        touch = originalEvent.changedTouches[0];
	        event.initMouseEvent(mouseEventTypes[originalEvent.type], true, true,
	                window, 0, touch.screenX, touch.screenY, touch.clientX,
	                touch.clientY, touch.ctrlKey, touch.altKey, touch.shiftKey,
	                touch.metaKey, 0, null);
	        originalEvent.target.dispatchEvent(event);
	    });
	};

    $( ".activity" ).draggable({ revert: true });

    $('.trash').droppable({
        over: function(event, ui) {
            $(ui.draggable).remove();
        }
    });

// },false);
})();